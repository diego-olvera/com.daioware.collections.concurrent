package com.daioware.collections.concurrent;

import java.util.Collection;
import java.util.LinkedList;

public class ConcurrentLinkedList<T> extends LinkedList<T>{
	private static final long serialVersionUID = 1L;

	public ConcurrentLinkedList() {
		super();
	}

	public ConcurrentLinkedList(Collection<? extends T> c) {
		super(c);
	}

	@Override
	public synchronized  T removeFirst() {
		return super.removeFirst();
	}

	@Override
	public synchronized  T removeLast() {
		return super.removeLast();
	}

	@Override
	public synchronized  void addFirst(T e) {
		super.addFirst(e);
	}

	@Override
	public synchronized  void addLast(T e) {
		super.addLast(e);
	}

	@Override
	public synchronized  boolean add(T e) {
		return super.add(e);
	}

	@Override
	public synchronized  boolean remove(Object o) {
		return super.remove(o);
	}

	@Override
	public synchronized  boolean addAll(Collection<? extends T> c) {
		return super.addAll(c);
	}

	@Override
	public synchronized  boolean addAll(int index, Collection<? extends T> c) {
		return super.addAll(index, c);
	}

	@Override
	public synchronized  void clear() {
		super.clear();
	}

	@Override
	public synchronized  T set(int index, T element) {
		return super.set(index, element);
	}

	@Override
	public synchronized  void add(int index, T element) {
		super.add(index, element);
	}

	@Override
	public synchronized  T remove(int index) {
		return super.remove(index);
	}

	@Override
	public synchronized  T remove() {
		return super.remove();
	}

	@Override
	public synchronized  boolean removeFirstOccurrence(Object o) {
		return super.removeFirstOccurrence(o);
	}

	@Override
	public synchronized  boolean removeLastOccurrence(Object o) {
		return super.removeLastOccurrence(o);
	}
	
}
